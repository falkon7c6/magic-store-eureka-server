package com.magic.magicstoreeurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MagicStoreEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicStoreEurekaServerApplication.class, args);
    }

}
